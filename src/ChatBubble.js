
import React, { Component } from 'react'
import { Container, Card } from 'semantic-ui-react'

export default class ChatArea extends Component {

  formatDate = (date) => {
    return (new Date(date.seconds * 1000)).toString().slice(4, 21)
  }

  render() {

    if (this.props.admin) {
      return (
        <Container fluid style={styles.bubbleAdminRow}>
          <Card style={styles.bubbleAdminContainer}>
            <div style={styles.bubbleText}>
              {this.props.text}
            </div>
          </Card>
          <div style={styles.timestamp}>
            {this.formatDate(this.props.time)}
          </div>
        </Container>
      )
    } else {
      return (
        <Container fluid style={styles.bubbleUserRow}>
          <Card style={styles.bubbleUserContainer}>
            <div style={styles.bubbleText}>
              {this.props.text}
            </div>
          </Card>
          <div style={styles.timestamp}>
            {this.formatDate(this.props.time)}
          </div>
        </Container>
      )
    }
  }
}

let styles = {
  bubbleAdminRow: {
    display: 'flex',
    flexDirection: 'row-reverse',
    marginBottom: 16
  },
  bubbleUserRow: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 16
  },
  bubbleAdminContainer: {
    minWidth: 0,
    maxWidth: '80%',
    borderRadius: 16,
    padding: 8,
    paddingLeft: 16,
    paddingRight: 16,
    width: 'fit-content',
    margin: 0

  },
  bubbleUserContainer: {
    minWidth: 0,
    maxWidth: '80%',
    borderRadius: 16,
    padding: 8,
    paddingLeft: 16,
    paddingRight: 16,
    width: 'fit-content',
    backgroundColor: '#555',
    color: '#fff',
    margin: 0
  },
  bubbleText: {
    fontSize: '1.1em'
  },
  timestamp: {
    alignSelf: 'flex-end',
    marginLeft: '8px',
    marginRight: '8px',
    color: '#aaa'
  }
}
