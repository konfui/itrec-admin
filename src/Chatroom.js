import React, { Component } from 'react'
import { Container, Form, Input } from 'semantic-ui-react'
import ChatArea from './ChatArea'
import ChatInput from './ChatInput'

export default class Chatroom extends Component {
  scrollDown() {
    this.arearef.scrollToBottom()
  }

  render() {
    let roomName = this.props.roomName
    let headerContent  = (
        <Form onSubmit={this.props.submitRoomNameChange} style={styles.container}>
          <h2 style={{display: 'inline'}}>
            <Input
              fluid
              transparent
              size='large'
              placeholder='Unnamed Room' 
              value={roomName} 
              onChange={this.props.changeRoomNameChange}
              onFocus={this.props.beginEdit}/></h2>
        </Form>
      )
    roomName = <div style={styles.roomHeader}>{headerContent}</div>
    return (
      <Container fluid style={styles.chatroomContainer}>
        {this.props.chatroom !== undefined ? roomName : undefined}
        <ChatArea ref={(ref) => {this.arearef = ref}} chatroom={this.props.chatroom}/>
        <ChatInput onSubmit={this.props.onSubmit} onFocus={this.props.onFocus}/>
      </Container>
    )
  }
}

let styles = {
  chatroomContainer: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 0
  },
  roomHeader: {
    padding: 16,
    display: 'inline',
    borderBottomStyle: 'solid',
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
  },
  nameField: {
    fontWeight: 'bold'
  }
}
