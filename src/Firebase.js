import firebase from 'firebase';
import 'firebase/firestore';

let config = {
    apiKey: 'AIzaSyBOX1uI3p6nN0XOTxR-P0xek-jajQj1Tng',
    projectId: 'qir-ftui'
};

let app = firebase.initializeApp(config);

firebase.firestore().settings({
    cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
});

firebase.firestore().enablePersistence().then(() => console.log("Persistence enabled"))
    .catch(function (err) {
        if (err.code === 'failed-precondition') {
            console.log("Persistence disabled")
        } else if (err.code === 'unimplemented') {
            console.log("Persistence unsupported")
        } else {
            console.log("Failed to enable persistence")
        }
    });

export const firestore = app.firestore();